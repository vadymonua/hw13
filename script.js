let m = Math.floor(Math.random() * 59)
if (m <= 15) {
    console.log(m, 'first quoter')
} else if (m > 15 && m < 30) {
    console.log(m, 'second quoter')
} else if (m > 30 && m < 45) {
    console.log(m, 'third quoter')
} else if (m > 45 && m <= 59) {
    console.log(m, 'four quoter')
} else {
    console.log(m, 'not a minute')
}


//-----------------------------//


let leng = 'en'

if (leng === 'en') {
    console.log(['mn', 'ts', 'wd', 'th', 'fr', 'st', 'sn'])
} else if (leng === 'ua') {
    console.log(['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'нд'])
} else {
    console.log("дана мова не підтримується")
}

switch (leng) {
    case 'ua':
        console.log(['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'нд']);
        break;
    case 'en':
        console.log(['mn', 'ts', 'wd', 'th', 'fr', 'st', 'sn']);
        break;
    default:
        console.log("sorry")

}
let obj = {
    'ua': ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'нд'],
    'en': ['mn', 'ts', 'wd', 'th', 'fr', 'st', 'sn']
}
console.log(obj[leng])

//------------------------------------//

let arr = []
for (let i = 1; i <= 10; i++) {
    arr.push(Math.floor(Math.random() * 10))
}
console.log(arr)
let sum = 0
for (let n = 0; n < 9; n++) {
    sum = sum + arr[n]
}
console.log(sum)



